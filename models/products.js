const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product Name is required"],
  },

  description: {
    type: String,
    required: [true, "Description is required"],
  },

  price: {
    type: Number,
    required: [true, "Price is required"],
  },

  stocks: {
    type: Number,
    required: [true, "Stock number is required"],
  },

  isActive: {
    type: Boolean,
    default: true,
  },

  createdOn: {
    type: Date,
    default: new Date(),
  },
  orders: [
    {

      userId: {
        type: String,
      },
      userEmail: {
        type: String,
      },
      quantity: {
        type: Number,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model("Product", productSchema);
