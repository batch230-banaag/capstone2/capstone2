const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required"],
  },
  lastName: {
    type: String,
    required: [true, "Last name is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile Number is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },

  totalAll: {
    type: Number,
  },
  
  orders: [
    {
      totalAmount: {
        type: Number,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
      products: [
        {
          productId: {
            type: String,
          },
          productName: {
            type: String,
          },
          quantity: {
            type: Number,
          },
        },
      ],
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
