const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;
const app = express();

app.use(cors());
app.use(express());
app.use(express.json());

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://admin:admin@batch230.yvvrpsv.mongodb.net/Capstone2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to Banaag-Capstone2-DB")
);

app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000}`);
});
