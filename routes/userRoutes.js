const express = require("express");
const router = express.Router();
const usersController = require("../controllers/usersController");
const productsController = require("../controllers/productsController");
const auth = require("../auth");

router.post("/register", usersController.registerUser);

router.get("/registeredUsers", usersController.getUsers);

router.post("/login", usersController.loginUser);

router.get("/:userId/userDetails", usersController.userDetails);

router.post("/checkOut", auth.verify, usersController.checkOut);

router.patch("/:userId/setAsAdmin", auth.verify, usersController.setAsAdmin);


router.patch("/quantity", auth.verify, usersController.changeQuantity);


module.exports = router;
