const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productsController");
const auth = require("../auth");

router.post("/addProduct", auth.verify, productsController.addProduct);

router.get("/allProducts", productsController.allProducts);

router.get("/specificProduct/:prodId", productsController.specificProduct);

router.put("/:prodId", auth.verify, productsController.updateProduct);

router.patch(
  "/:prodId/archive",
  auth.verify,
  productsController.archiveProduct
);

module.exports = router;
