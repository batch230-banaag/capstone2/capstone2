const User = require("../models/users");
const Order = require("../models/orders");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/products");

module.exports.registerUser = async (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    mobileNo: req.body.mobileNo,
  });

  console.log(newUser);

  return await newUser
    .save()
    .then((user) => {
      console.log(user);
      res.send("User registration successful!");
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.getUsers = async (req, res) => {
  return await User.find({})
    .then((result) => {
      res.send(result);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.userDetails = async (req, res) => {
  return await User.findById(req.params.userId)
    .then((result) => {
      result.password = "";
      res.send(result);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.loginUser = async (req, res) => {
  return await User.findOne({ email: req.body.email })
    .then((result) => {
      if (result == null) {
        res.send("Email does not exist!");
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          result.password
        );

        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(result) });
        } else {
          return res.send("Password is incorrect. Please try again.");
        }
      }
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.setAsAdmin = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return await User.findByIdAndUpdate(
      req.params.userId,
      {
        isAdmin: req.body.isAdmin,
      },
      {
        new: true,
      }
    )
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.send(error);
      });
  } else {
    return res.send("User must be ADMIN to access this functionality");
  }
};

module.exports.checkOut = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let productName = await Product.findById(req.body.productId).then(
    (result) => result.name
  );

  let productPrice = await Product.findById(req.body.productId).then(
    (result) => result.price
  );

  if (userData.isAdmin != true) {
    let data = {
      userId: userData.id,
      email: userData.email,
      productId: req.body.productId,
      productName: productName,
      productPrice: productPrice,
      quantity: req.body.quantity,
    };

    console.log(data.userId);

    let isUserUpdated = await User.findById(data.userId).then((user) => {
      user.totalAmount = data.quantity * data.productPrice;

      user.orders.push({
        totalAmount: user.totalAmount,
        products: [
          {
            productId: data.productId,
            productName: data.productName,
            quantity: data.quantity,
          },
        ],
      });

      var totalAllInitial = 0;
      user.orders.forEach((totalAllInitial) => {
        totalAllInitial += user.orders.totalAmount;
      });

      return user.save().catch((error) => {
        console.log(error);
        return false;
      });
    });

    console.log(isUserUpdated);

    let isProductUpdated = await Product.findById(data.productId).then(
      (product) => {
        product.orders.push({
          userId: data.userId,
          userEmail: data.email,
          quantity: data.quantity,
        });

        product.stocks = product.stocks - data.quantity;

        return product.save().catch((error) => {
          console.log(error);
          return false;
        });
      }
    );

    console.log(isProductUpdated);

    isUserUpdated && isProductUpdated
      ? res.send("User and Product details updated!")
      : res.send(false);
  } else {
    return res
      .status(401)
      .send("User must be NON-ADMIN to access this functionality");
  }
};

// CHANGE PRODUCT QUANTITY
module.exports.changeQuantity = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(userData.id);

  let productPrice = await Product.findById(req.body.productId).then(
    (result) => result.price
  );

  User.findByIdAndUpdate(
    userData.id,
    {
      "orders.$[order].products.$[product].quantity": req.body.quantity,
      "orders.$[order].totalAmount": req.body.quantity * productPrice,
    },

    {
      arrayFilters: [
        {
          "order._id": req.body.orderId,
        },
        {
          "product._id": req.body.orderProductId,
        },
      ],
      new: true,
    }
  )
    .then((result) => {
      res.send(result);
    })
    .catch((error) => {
      res.send(error);
    });

  Product.findByIdAndUpdate(req.body.productId, {});
};
