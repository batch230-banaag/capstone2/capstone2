const Product = require("../models/products");
const auth = require("../auth");
const products = require("../models/products");

module.exports.addProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    stocks: req.body.stocks,
  });

  if (userData.isAdmin) {
    return await newProduct
      .save()
      .then((product) => {
        console.log(product);
        res.send(product);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  } else {
    return res
      .status(401)
      .send("User must be ADMIN to access this functionality");
  }
};

module.exports.allProducts = async (req, res) => {
  return await Product.find({isActive: true})
    .then((products) => {
      console.log(products);
      res.send(products);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.specificProduct = async (req, res) => {
  return await Product.findById(req.params.prodId)
    .then((products) => {
      console.log(products);
      res.send(products);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};

module.exports.updateProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    let updatedProduct = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
    };
    return await Product.findByIdAndUpdate(req.params.prodId, updatedProduct, {
      new: true,
    })
      .then((result) => {
        console.log(result);
        res.send(result);
      })
      .catch((error) => {
        console.log(error);
        res.send(false);
      });
  } else {
    return res
      .status(401)
      .send("User must be ADMIN to access this functionality");
  }
};

module.exports.archiveProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    return await Product.findByIdAndUpdate(
      req.params.prodId,
      {
        isActive: req.body.isActive,
      },
      {
        new: true,
      }
    )
      .then((result) => {
        console.log(result);
        res.send(result);
      })
      .catch((error) => {
        console.log(error);
        res.send(false);
      });
  } else {
    return res
      .status(401)
      .send("User must be ADMIN to access this functionality");
  }
};
